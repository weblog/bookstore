from django.db import models
from django.forms import ModelForm
from stock.models import *


# Create your models here.
class Order(models.Model):

    book = models.ForeignKey('stock.Book')
    order_on = models.DateTimeField(auto_now_add=True)
    quantity = models.IntegerField()

    def __unicode__(self):
        return "%s %d ea. %s" % (self.book.name, self.quantity, self.order_on)

class OrderForm(ModelForm):
    class Meta:
        model = Order
