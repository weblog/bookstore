from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from ordering.models import *
from stock.models import *

@login_required
def index(request):
    data = {}
    if request.method == 'POST':
        order_form = OrderForm(request.POST)
        order = order_form.save()
        book = order.book
        book.quatity = book.quatity + order.quantity
        book.save()
        Stock(book=book, quantity=order.quantity, remain_book=book.quatity).save()
    form = OrderForm()
    data['form'] = form
    return render(request, 'ordering.html', data)
