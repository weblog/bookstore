from fabric.api import *


def start():
    local('python manage.py runserver 0.0.0.0:8000')


def sync():
    with open('fab_url', 'r') as f:
        local('python manage.py sync_bookstore %s' % f.read())
