from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from stock.models import *
from datetime import datetime, timedelta

@login_required
def index(request):
    return render(request, 'index.html', {})

@login_required
def stock_report(request):
    data = {
        'test_date': Stock.objects.first().create_on,
    }
    if request.POST:
        input_date = request.POST.get('date')
        if input_date:
            start_date = datetime.strptime(input_date, '%Y-%m-%d')
            end_date = start_date + timedelta(days=1)
            stocks = Stock.objects.filter(create_on__range=[start_date, end_date]).order_by('create_on')
            data['input_date'] = start_date.date()
            data['stocks'] = stocks
        print input_date
    return render(request, 'stock_report.html', data)

@login_required
def list_books_by_author(request):
    data = {'authors': list()}
    authors_obj = Author.get_all_authors()
    for author in authors_obj:
        data['authors'].append([author.name, Book.get_book_by_author(author)])
    return render(request, 'list_books_by_author.html', data)

@login_required
def all_books(request):
    data = {}
    data['books'] = Book.objects.all().order_by('name')
    return render(request, 'all_books.html', data)
