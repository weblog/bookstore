#!/usr/bin/python
# - * - coding: utf-8 - * -

from django.core.management.base import BaseCommand
from stock.models import *

import requests


class Command(BaseCommand):
    args = '<url_to_sync>'
    help = 'sync database from outside'

    def handle(self, *args, **options):
        '''
            Handle Function
        '''
        # get data from url
        print "[+] Getting data from url "
        data = self.get_json_from_url(args[0])
        if not data:
            raise Exception('Cannot get data from url as json')
        data_len = len(data)
        authors = set(map(lambda x: x[2], data))
        print "[+] Add authors to Author Model"
        if not self.add_authors_list_to_model(authors):
            raise Exception('Cannot insert authors')
        if not self.update_books(data, data_len):
            raise Exception('Cannot insert books')

    def get_json_from_url(self, url):
        return requests.get(url).json()

    def add_authors_list_to_model(self, authors):
        print "[+] Get all data from Author to and difference set"
        db_authors = set(Author.objects.values_list('name', flat=True))
        authors = authors - db_authors
        authors_obj = []
        for author in authors:
            authors_obj.append(Author(name=author))
        print "[+] Making author name to objects"
        print "[+] Inserting author to database "
        if authors_obj:
            Author.objects.bulk_create(authors_obj)
            print "[+] Objects inserted "
        else:
            print "[-] No authors inserted "
        return True

    def update_books(self, data_list, data_len):
        book_to_insert = []
        for data in data_list:
            try:
                book = Book.objects.get(pk=data[0])
                author = Author.objects.get(name=data[2])
                book.name = data[1]
                book.author = author
                book.price = data[3]
                book.save()
            except Book.DoesNotExist:
                author = Author.objects.get(name=data[2])
                book_to_insert.append(Book(pk=data[0], name=data[1], quatity=0, author=author, price=data[3]))
        print "[+] Updated book "
        if book_to_insert:
            Book.objects.bulk_create(book_to_insert)
            print "[+] Created new book "
        return True
