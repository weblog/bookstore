from django.contrib import admin
from stock.models import *


# Register your models here.
admin.site.register(Book)
admin.site.register(Stock)
admin.site.register(Author)
