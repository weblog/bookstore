# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Book'
        db.create_table(u'stock_book', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('price', self.gf('django.db.models.fields.FloatField')()),
            ('quatity', self.gf('django.db.models.fields.IntegerField')()),
            ('author', self.gf('django.db.models.fields.related.ForeignKey')(related_name='books', to=orm['stock.Author'])),
        ))
        db.send_create_signal(u'stock', ['Book'])

        # Adding model 'Author'
        db.create_table(u'stock_author', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'stock', ['Author'])

        # Adding model 'Stock'
        db.create_table(u'stock_stock', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('book', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['stock.Book'])),
            ('quantity', self.gf('django.db.models.fields.IntegerField')()),
            ('create_on', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('remain_book', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'stock', ['Stock'])


    def backwards(self, orm):
        # Deleting model 'Book'
        db.delete_table(u'stock_book')

        # Deleting model 'Author'
        db.delete_table(u'stock_author')

        # Deleting model 'Stock'
        db.delete_table(u'stock_stock')


    models = {
        u'stock.author': {
            'Meta': {'object_name': 'Author'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'stock.book': {
            'Meta': {'object_name': 'Book'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'books'", 'to': u"orm['stock.Author']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'price': ('django.db.models.fields.FloatField', [], {}),
            'quatity': ('django.db.models.fields.IntegerField', [], {})
        },
        u'stock.stock': {
            'Meta': {'object_name': 'Stock'},
            'book': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['stock.Book']"}),
            'create_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'quantity': ('django.db.models.fields.IntegerField', [], {}),
            'remain_book': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['stock']