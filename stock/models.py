from django.db import models


# Create your models here.
class Book(models.Model):

    name = models.CharField(max_length=255)
    price = models.FloatField()
    quatity = models.IntegerField()
    author = models.ForeignKey('Author', related_name="books")

    def __unicode__(self):
        return self.name

    @staticmethod
    def get_book_by_author(author):
       return Book.objects.all().filter(author__exact=author).order_by('name')


class Author(models.Model):

    name = models.CharField(max_length=255)

    def __unicode__(self):
        return self.name

    @staticmethod
    def get_all_authors():
        return Author.objects.all().order_by('name')


class Stock(models.Model):

    book = models.ForeignKey('Book')
    quantity = models.IntegerField()
    create_on = models.DateTimeField(auto_now_add=True)
    remain_book = models.IntegerField()

    def __unicode__(self):
        return unicode(self.remain_book)

    @property
    def quantity_order(self):
        return self.quantity if self.quantity > 0 else None

    @property
    def quantity_sale(self):
        return -self.quantity if self.quantity < 0 else None
