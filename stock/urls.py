# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns(
    'stock.views',
    url(r'^$', 'index', name='index'),
    url(r'^stock_report$', 'stock_report', name='stock_report'),
    url(r'^authors/$', 'list_books_by_author', name='authors'),

    url(r'^books/$','all_books',name="all_books"),
)
