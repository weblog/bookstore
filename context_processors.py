#!/usr/bin/python
# -*- coding: utf-8


class Menu(object):
    def __init__(self, name, url, submenu=None):
        self.name = name
        self.url = url
        self.submenu = submenu

    def __unicode__(self):
        return self.name


def context(request):
    context = {}
    if 'menu' not in request:
        stock_menu = Menu('Stock', '/stock')
        order_menu = Menu('Ordering','/ordering')
        pos_menu = Menu('Point of sale','/pos/index')
        register_vip = Menu('Register VIP','/pos/register_vip')
        pos_menu.submenu = [Menu('Point','/pos'), Menu('Report','/pos/sale_report')]
        stock_menu.submenu = [Menu('Report', '/stock/stock_report'), Menu('Author', '/stock/authors'), Menu('Books', '/stock/books')]
        ''' TODO: Implement here! '''
        context['menu'] = [stock_menu, pos_menu, order_menu, register_vip]

    return context
