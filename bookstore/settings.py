"""
Django settings for bookstore project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
from unipath import Path
import json
import os


BASE_DIR = Path(__file__).absolute().ancestor(2)
# BASE_DIR = os.path.dirname(os.path.dirname(__file__))


try:
    with open(BASE_DIR.child('secrets.json')) as handle:
        SECRETS = json.load(handle)
except IOError:
    SECRETS = {}


DEBUG = SECRETS.get('debug', False)
TEMPLATE_DEBUG = SECRETS.get('debug', False)

ALLOWED_HOSTS = SECRETS.get('allowed_hosts', [])

SERVER = str(SECRETS.get('server', None))

# Database Settings
if not SECRETS.get('engine') or SECRETS.get('engine') == 'sqlite3':
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',
            'NAME': SECRETS.get('db_name', os.path.join(BASE_DIR, 'db.sqlite3')),
        }
    }
elif SECRETS.get('engine') == 'mysql':
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': SECRETS.get('db_name', 'dbname'),
            'USER': SECRETS.get('db_user', 'root'),
            'PASSWORD': SECRETS.get('db_password', 'password'),
            'HOST': SECRETS.get('db_host', '127.0.0.1'),
        }
    }
SECRET_KEY = SECRETS.get('secret_key', 'm)5)of34!)5xx#8&k6zfk2t5e(kqp1_y$ox81%uq=et!$@ig#$')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    'south',
    'ordering',
    'pos',
    'stock',
    'picker',
)

PICKER_INSTALLED_APPS = (
    'jquery',
    'bootstrap',
    'less',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'bookstore.urls'

WSGI_APPLICATION = 'bookstore.wsgi.application'

LANGUAGE_CODE = 'th'

TIME_ZONE = 'Asia/Bangkok'

DATE_FORMAT = "SHORT_DATE_FORMAT"

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/static/'

STATICFILES_DIRS = [BASE_DIR.child('static')]

TEMPLATE_DIRS = [os.path.join(BASE_DIR, 'templates')]

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.contrib.auth.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.static",
    "django.core.context_processors.tz",
    "django.contrib.messages.context_processors.messages",
    "context_processors.context",
)

LOGIN_REDIRECT_URL = '/'
LOGIN_URL = '/login/'
