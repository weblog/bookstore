from django.conf.urls import patterns, include, url
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.contrib.auth import views as auth_views
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'bookstore.views.index', name='index'),

    url(r'^stock/', include('stock.urls')),
    url(r'^ordering', include('ordering.urls')),
    url(r'^pos/', include('pos.urls')),
    url(r'^admin/', include(admin.site.urls)),

    url(r'^login/$', auth_views.login, {'template_name': 'login.html'}, name='auth_login'),
    url(r'^logout/$', auth_views.logout, {'next_page': '/'}, name='auth_logout'),
)
