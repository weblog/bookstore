from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from stock.models import *
from pos.models import *
from datetime import datetime, timedelta

@login_required()
def index(request):
    data = {
        'results': SaleReport().sale_report(datetime.now())
    }

    return render(request, 'index.html', data)
