from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from stock.models import *
from pos.models import *
from datetime import *
# Create your views here.

@login_required
def index(request):
    #datetime.datetime(2014,2,2)
    #results = SaleReport().sale_report(datetime.now())
    data = {}
    sale_form = SaleReportForm()
    if request.method == 'POST':
        sale_form = SaleReportForm(request.POST)
        
        if sale_form.is_valid():

            sale = sale_form.save(commit=False)
            sale.sale_value = sale.book.price * sale.quantity

            if sale.member:
                sale.sale_value *=0.9

            sale.save()

            book = sale.book
            book.quatity = book.quatity - sale.quantity
            book.save()
            ## wait for pay
            Stock(book=book,quantity= -sale.quantity, remain_book=book.quatity).save()

    data['form'] = sale_form

    return render(request, 'point_of_sale.html', data )

@login_required
def register_vip(request):
    data = {} 
    data['pay'] = VipForm()
    if request.POST:
        vip_regis = VipForm(request.POST)
        if vip_regis.is_valid():
            vip_regis.save()
        else:
            data['pay'] = vip_regis

    return render(request, 'regis_vip.html', data)

@login_required
def sale_report(request):
    data = {}
    if request.POST:
        if 'input_date' in request.POST:
            x = []
            x = map(int,request.POST['input_date'].split('-'))
            sort_by_date = SaleReport().sale_report(datetime(x[0],x[1],x[2]))
            data['pay'] = sort_by_date
    return render(request, 'sale_report.html', data)
