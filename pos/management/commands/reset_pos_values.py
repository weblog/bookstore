#!/usr/bin/python
# - * - coding: utf-8 - * -

from django.core.management.base import BaseCommand
from pos.models import *

import requests


class Command(BaseCommand):
    help = 'reset sale report values'

    def handle(self, *args, **options):
        '''
            Handle Function
        '''
        print "Reset is starting ..."
        for s in SaleReport.objects.all():
            s.sale_value = s.book.price * s.quantity
            if s.member:
                s.sale_value *= 0.9
            s.save()

        print "Finished !!"