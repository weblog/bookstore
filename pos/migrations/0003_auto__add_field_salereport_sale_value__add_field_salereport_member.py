# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'SaleReport.sale_value'
        db.add_column(u'pos_salereport', 'sale_value',
                      self.gf('django.db.models.fields.FloatField')(default=111),
                      keep_default=False)

        # Adding field 'SaleReport.member'
        db.add_column(u'pos_salereport', 'member',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=None, to=orm['pos.VIPmember'], null=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'SaleReport.sale_value'
        db.delete_column(u'pos_salereport', 'sale_value')

        # Deleting field 'SaleReport.member'
        db.delete_column(u'pos_salereport', 'member_id')


    models = {
        u'pos.salereport': {
            'Meta': {'object_name': 'SaleReport'},
            'book': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['stock.Book']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'member': ('django.db.models.fields.related.ForeignKey', [], {'default': 'None', 'to': u"orm['pos.VIPmember']", 'null': 'True'}),
            'quantity': ('django.db.models.fields.IntegerField', [], {}),
            'sale_value': ('django.db.models.fields.FloatField', [], {}),
            'sell_on': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'pos.vipmember': {
            'Meta': {'object_name': 'VIPmember'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'phone_no': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'stock.author': {
            'Meta': {'object_name': 'Author'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'stock.book': {
            'Meta': {'object_name': 'Book'},
            'author': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'books'", 'to': u"orm['stock.Author']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'price': ('django.db.models.fields.FloatField', [], {}),
            'quatity': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['pos']