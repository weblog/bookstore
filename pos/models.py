from django.db import models
from django.forms import ModelForm
from stock.models import *
from itertools import groupby
from datetime import *

# Create your models here.

class VIPmember(models.Model):

    name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    phone_no = models.CharField(max_length=50)

    def __unicode__(self):
        return "%s %s , %s" % (self.name,self.last_name,self.phone_no)
        #return "%s%s,%s" % (self.name[0].upper(), self.last_name[0].upper(), self.phone_no)

class SaleReport(models.Model):

    book = models.ForeignKey('stock.Book')
    quantity = models.IntegerField()
    sell_on = models.DateTimeField(auto_now_add=True)

    sale_value = models.FloatField()

    member = models.ForeignKey('pos.VIPmember', null=True , blank=True, default=None)

    def sale_report(self,post_date):
        start_date = post_date.date()
        end_date = start_date + timedelta(days = 1)

        SOP = SaleReport.objects.filter(sell_on__range=(start_date,end_date))

        SOP = [ (i.sell_on, i.book.name, i.quantity, i.sale_value ) for i in SOP ]
        SOP.sort(key=lambda (date,name,qty,value): (name,date))
        

       
        result = []
        last_name = ""
        for sop in SOP:
            name = sop[1]
            if name!=last_name:
                result.append([name,[],0,0])
            result[-1][1].append(sop)

            last_name = name


        for i in xrange(len(result)):
            result[i][2] = reduce(lambda x,y: x+y[2],result[i][1],0)
            result[i][3] = reduce(lambda x,y: x+y[3],result[i][1],0)
        #sum = reduce(lambda x,y: x+y[2],SOP,0)

        #print sum
        SOP = []
        last_C = ""
        for sop in result:
            name,books,totals,values = sop

            C = name[0].upper()
            #print C,last_C,C, "########"
            if C!=last_C:
                SOP.append([C,[]])
            SOP[-1][1].append(sop)

            last_C = C

        return SOP
        
    def __unicode__(self):
        return "%s %d ea. %s : %.2f" % (self.book.name, self.quantity, self.sell_on , self.sale_value)

class SaleReportForm(ModelForm):
    class Meta:
        model = SaleReport
        exclude = [ 'sale_value']
        
class VipForm(ModelForm):
    class Meta:
        model = VIPmember
        
        

