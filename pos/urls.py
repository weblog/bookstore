# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('pos.views',
    url(r'^$', 'index', name='index'),
    url(r'^sale_report/', 'sale_report', name="sale_report"),
    url(r'^index/', 'index', name="pos"),
    url(r'^register_vip', 'register_vip', name="register_vip"),
)
